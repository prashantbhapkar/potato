from dagger import function, object_type
import os

@object_type
class Potato:
 @function
 def hello_world(self) -> str:
  return "Hello Daggernauts!"

 @function
 def print_version(self) -> str:
  # **Place your code for accessing version.txt here**

  version_file_path = "version.txt"

  # Check if the file exists
  if os.path.isfile(version_file_path):
   # Read version number from file
   with open(version_file_path, "r") as file:
    version_number = file.read().strip()
   return version_number
  else:
   return "Version file not found"
